# -*- coding: utf-8 -*-

import tarfile
import time
import codecs
import os
import shutil
import sys
import re
from builtins import input
from builtins import object
from datetime import datetime
from pathlib import Path

# ------------------------------------AHOCORASICK SEARCH LIBRARIES-----------------------------------------------------

# AhoCorasick implementation entirely written in python.
# Supports unicode.
#
# Quite optimized, the code may not be as beautiful as you like,
# since inlining and so on was necessary
#
# Created on Jan 5, 2016
#
# @author: Frederik Petersen (fp@abusix.com)
# ahocorapy AhoCorasick search https://github.com/abusix/ahocorapy
# https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm
#
# modified slightly by Andrew Smith 4/19/2022 to return the some extra values you pass into it
# ...did not change the search any, only that the result will include an extra un-processed parameter we pass into it
# ----------------------------------------------------------------------------------------------------------------------


class State(object):
    __slots__ = ['identifier', 'symbol', 'success', 'transitions', 'parent',
                 'matched_keyword', 'longest_strict_suffix', 'original_term_from_keyword']

    def __init__(self, identifier, symbol=None,  parent=None, success=False):
        self.symbol = symbol
        self.identifier = identifier
        self.transitions = {}
        self.parent = parent
        self.success = success
        self.matched_keyword = None
        self.longest_strict_suffix = None
        self.original_term_from_keyword = None

    def __str__(self):
        transitions_as_string = ','.join(
            ['{0} -> {1}'.format(key, value.identifier) for key, value in self.transitions.items()])
        return "State {0}. Transitions: {1}".format(self.identifier, transitions_as_string)


class KeywordTree(object):

    def __init__(self, case_insensitive=False):
        """
        @param case_insensitive: If true, case will be ignored when searching.
                                 Setting this to true will have a positive
                                 impact on performance.
                                 Defaults to false.
        @param over_allocation: Determines how big initial transition arrays
                                are and how much space is allocated in addition
                                to what is essential when array needs to be
                                resized. Default value 2 seemed to be sweet
                                spot for memory as well as cpu.
        """
        self._zero_state = State(0)
        self._counter = 1
        self._finalized = False
        self._case_insensitive = case_insensitive
        self._full_term_from_keyword = ''

    def add(self, keyword, full_word):
        """
        Add a keyword to the tree.
        Can only be used before finalize() has been called.
        Keyword should be str or unicode.
        """
        if self._finalized:
            raise ValueError('KeywordTree has been finalized.' +
                             ' No more keyword additions allowed')

        original_keyword = keyword

        if self._case_insensitive:
            keyword = keyword.lower()
        if len(keyword) <= 0:
            return
        current_state = self._zero_state
        for char in keyword:
            try:
                current_state = current_state.transitions[char]
            except KeyError:
                next_state = State(self._counter, parent=current_state,
                                   symbol=char)
                self._counter += 1
                current_state.transitions[char] = next_state
                current_state = next_state
        current_state.success = True
        current_state.matched_keyword = original_keyword
        current_state.original_term_from_keyword = full_word

    def search(self, text):
        """
        Alias for the search_one method
        """
        return self.search_one(text)

    def search_one(self, text):
        """
        Search a text for any occurrence of any added keyword.
        Returns when one keyword has been found.
        Can only be called after finalized() has been called.
        O(n) with n = len(text)
        @return: 2-Tuple with keyword and startindex in text.
                 Or None if no keyword was found in the text.
        """
        result_gen = self.search_all(text)
        try:
            return next(result_gen)
        except StopIteration:
            return None

    def search_all(self, text):
        """
        Search a text for all occurrences of the added keywords.
        Can only be called after finalized() has been called.
        O(n) with n = len(text)
        @return: Generator used to iterate over the results.
                 Or None if no keyword was found in the text.
        """
        if not self._finalized:
            raise ValueError('KeywordTree has not been finalized.' +
                             ' No search allowed. Call finalize() first.')
        if self._case_insensitive:
            text = text.lower()
        zero_state = self._zero_state
        current_state = zero_state
        for idx, symbol in enumerate(text):
            current_state = current_state.transitions.get(
                symbol, zero_state.transitions.get(symbol, zero_state))
            state = current_state
            while state is not zero_state:
                if state.success:
                    keyword = state.matched_keyword
                    original_term_from_keyword = state.original_term_from_keyword
                    yield keyword, original_term_from_keyword
                state = state.longest_strict_suffix

    def finalize(self):
        """
        Needs to be called after all keywords have been added and
        before any searching is performed.
        """
        if self._finalized:
            raise ValueError('KeywordTree has already been finalized.')
        self._zero_state.longest_strict_suffix = self._zero_state
        self.search_lss_for_children(self._zero_state)
        self._finalized = True

    def search_lss_for_children(self, zero_state):
        processed = set()
        to_process = [zero_state]
        while to_process:
            state = to_process.pop()
            processed.add(state.identifier)
            for child in state.transitions.values():
                if child.identifier not in processed:
                    self.search_lss(child)
                    to_process.append(child)

    def search_lss(self, state):
        zero_state = self._zero_state
        parent = state.parent
        traversed = parent.longest_strict_suffix
        while True:
            if state.symbol in traversed.transitions and \
                    traversed.transitions[state.symbol] is not state:
                state.longest_strict_suffix = \
                    traversed.transitions[state.symbol]
                break
            elif traversed is zero_state:
                state.longest_strict_suffix = zero_state
                break
            else:
                traversed = traversed.longest_strict_suffix
        suffix = state.longest_strict_suffix
        if suffix is zero_state:
            return
        if suffix.longest_strict_suffix is None:
            self.search_lss(suffix)
        for symbol, next_state in suffix.transitions.items():
            if symbol not in state.transitions:
                state.transitions[symbol] = next_state

    def __str__(self):
        return "ahocorapy KeywordTree"

    def __getstate__(self):
        state_list = [None] * self._counter
        todo_list = [self._zero_state]
        while todo_list:
            state = todo_list.pop()
            transitions = {key: value.identifier for key, value in state.transitions.items()}
            state_list[state.identifier] = {
                'symbol': state.symbol,
                'success': state.success,
                'parent':  state.parent.identifier if state.parent is not None else None,
                'matched_keyword': state.matched_keyword,
                'longest_strict_suffix': state.longest_strict_suffix.identifier if state.longest_strict_suffix is not None else None,
                'transitions': transitions,
                'original_term_from_keyword': state.original_term_from_keyword
            }
            for child in state.transitions.values():
                if len(state_list) <= child.identifier or not state_list[child.identifier]:
                    todo_list.append(child)

        return {
            'case_insensitive': self._case_insensitive,
            'finalized': self._finalized,
            'counter': self._counter,
            'states': state_list
        }

    def __setstate__(self, state):
        self._case_insensitive = state['case_insensitive']
        self._counter = state['counter']
        self._finalized = state['finalized']
        states = [None] * len(state['states'])
        for idx, serialized_state in enumerate(state['states']):
            deserialized_state = State(idx, serialized_state['symbol'])
            deserialized_state.success = serialized_state['success']
            deserialized_state.matched_keyword = serialized_state['matched_keyword']
            states[idx] = deserialized_state
        for idx, serialized_state in enumerate(state['states']):
            deserialized_state = states[idx]
            if serialized_state['longest_strict_suffix'] is not None:
                deserialized_state.longest_strict_suffix = states[
                    serialized_state['longest_strict_suffix']]
            else:
                deserialized_state.longest_strict_suffix = None
            if serialized_state['parent'] is not None:
                deserialized_state.parent = states[serialized_state['parent']]
            else:
                deserialized_state.parent = None
            deserialized_state.transitions = {
                key: states[value] for key, value in serialized_state['transitions'].items()}
        self._zero_state = states[0]


# ------------------------------------INPUT AND LIBRARIES SECTION------------------------------------------------------

print('Confluence Attachment Fixing Tool v1.9.0 by Dave Norton and Andrew Smith [Atlassian] (2016-2022)')
print('  ----------------------------------------------------------------------------------------------- ')
print('Please ensure Confluence has been shut down and you have a full backup of the attachments directory')
print('  ----------------------------------------------------------------------------------------------- ')

# Grab our attachment path
attachmentPath = input('Where is your Confluence attachments directory? : ')
assert isinstance(attachmentPath, str)
print(' ')

# Let's check what the separator is?
print('What is your column separator?')
colSeparator = input('Type "t" to use a tab character or type the character you\'re using such as "," or "|" ')
if str.lower(colSeparator) == 't':
    print('Setting your column separator to a tab character')
    print(' ')
    colSeparator = "\t"
else:
    print('Setting your column separator to ' + colSeparator)
    print(' ')

# Back up the previous results
print('Backing up previous results...')
print(' ')
if not os.path.exists('results_backups'):
    os.makedirs('results_backups')
timestr = time.strftime("%Y%m%d-%H%M%S")
tar = tarfile.open("results_backups/results_FindMissingAttachments." + timestr + '.tar.gz', "w:gz")
for name in ["MissingAttachments.txt", "PathsToCheck.txt", "FoundAttachments.txt", "AttachmentsDirContains.txt", "fileConflicts.txt"]:
    tar.add(name)

# Clear out the files we're going to write to so we have a clean slate:
tar.close()
open('MissingAttachments.txt', 'w').close()
open('PathsToCheck.txt', 'w').close()
open('FoundAttachments.txt', 'w').close()
open('fileConflicts.txt', 'w').close()

# If there's a trailing slash, get rid of it:
if attachmentPath.endswith('/'):
    attachmentPath = attachmentPath[:-1]

# Check the attachments directory is acceptable:
if not (os.path.isdir(os.path.join(attachmentPath, 'ver003'))):
    print('[Error] - The directory "%s" is not a valid attachments directory' % attachmentPath)
    sys.exit(1)

# Make sure our required files are present:
for required_file in ['Attachments.txt']:
    if not os.path.exists(required_file):
        print('[Error] Could not find %s' % required_file)
        sys.exit(1)


# This is our mod function
def mod(the_id):
    the_id = int(the_id)
    firstFolder = int(the_id % 250)
    secondFolder = int(((the_id - (the_id % 1000)) / 1000) % 250)
    return os.path.join(str(firstFolder), str(secondFolder), str(the_id))


# a Function for spitting a list to disk
def listtodisk(thelist, l2dfilename):
    with open(l2dfilename, 'w') as F:
        for item in thelist:
            F.write("%s\n" % item)
        F.close()


# a function for zapping the Byte Order Marker
# based on http://stackoverflow.com/questions/8898294/convert-utf-8-with-bom-to-utf-8-with-no-bom-in-python
# as well as https://github.com/terencez127/BomSweeper
def bomzap(bzfilename):
    BUFSIZE = 4096
    BOMLEN = len(codecs.BOM_UTF8)

    with open(bzfilename, 'r+b') as fp:
        chunk = fp.read(BUFSIZE)
        if chunk.startswith(codecs.BOM_UTF8):
            bzi = 0
            chunk = chunk[BOMLEN:]
            while chunk:
                fp.seek(bzi)
                fp.write(chunk)
                bzi += len(chunk)
                fp.seek(BOMLEN, os.SEEK_CUR)
                chunk = fp.read(BUFSIZE)
            fp.seek(-BOMLEN, os.SEEK_CUR)
            fp.truncate()
    fp.close()


def handleEscapes(heterm):
    j = heterm.replace("\\", "\\\\")
    return j


def getKeyword(path):
    j = os.sep + path.split(os.sep)[-2] + os.sep + path.split(os.sep)[-1]
    j = j.encode("ascii", "ignore")
    j = j.decode("ascii")
    return j.encode("utf-8").decode("utf-8")


def regexSearchTerm(researchterm):
    reSearchTerm = r".*" + handleEscapes(getKeyword(researchterm)) + r"$"
    return reSearchTerm


def fileOrNot(fileornotpath):
    return os.path.isfile(os.path.normpath(fileornotpath))


def input_yesno(prompt):
    true_false = False
    full_prompt = prompt + ' [y]es / [n]o : '
    while True:
        answer = input(full_prompt).strip()
        if answer != '':
            answer = str(answer[0].lower())
            if answer == 'y':
                true_false = True
                return true_false
            if answer == 'n':
                true_false = False
                return true_false
            print('INPUT ERROR - please type "y" or "n"')


# --------------------------------BUILD LIST OF ATTACHMENTS SECTION----------------------------------------------------


# A list to store our paths to check:
pathsToCheck = []
tic = time.time()


# Let's generate some paths!
print('Generating paths based on IDs in Attachments.txt.... This may take a while - stand by!')

# Zap the BOM (if any) in Attachments.txt
print('Cleaning up the Byte Order Marker in Attachments.txt if any')
bomzap('Attachments.txt')
numPaths = 0
with open('Attachments.txt') as f:
    for i, line in enumerate(f):
        # A minimum viable line has a length of 5 - 3 single digits and two 1 character separators:
        if len(line) < 5:
            print('[INFO] Line %s appears to be an empty or invalid line - ignoring it!' % (i+1))
            continue

        # Split the line (if we can):
        ids = line.strip().split(colSeparator)

        # Check if the line matches the spec:
        if len(ids) != 4:
            print('[ERROR] Line %s does not conform to the spec:' % (i+1))
            sys.exit('spaceid' + colSeparator + 'pageid' + colSeparator + 'contentid' + colSeparator + 'version')

        # Then, make sure we have no null/empty values
        if len(ids[0]) == 0 or len(ids[1]) == 0 or len(ids[2]) == 0:
            sys.exit('[ERROR] Line %s has an empty value for an ID. Delete this line from Attachments.txt' % (i+1))

        # Is the line a header row?
        if ids[0].isalpha() or ids[1].isalpha() or ids[2].isalpha():
            sys.exit('[ERROR] Line %s looks like a header row' % (i+1))

        # Create a path based on each id:
        space = ids[0]
        page = ids[1]
        content = ids[2]
        version = ids[3]

        # Whip up a path - making sure to account for extra spaces around the content ID and file version:
        pathsToCheck.append(
            os.path.join(attachmentPath.strip(), 'ver003', mod(space), mod(page), content.strip(), version.strip()))

        sys.stdout.write('\rGenerating path # ' + str(i + 1))
        sys.stdout.flush()
        numPaths += 1
    toc = time.time()
    timeToGeneratePathsToCheck = round((toc - tic), 3)
    print(' ')
    print('--generating ' + str(numPaths) + ' attachment(s)\' paths took ' + str(timeToGeneratePathsToCheck) + ' seconds')
    print(' ')
f.close()
# Write the attachment paths to check to a file:
print('Writing the full list of attachment paths to check in PathsToCheck.txt')
listtodisk(pathsToCheck, 'PathsToCheck.txt')

# ---------------------------- CHECK IF ATTACHMENTS ARE THERE OR NOT ---------------------------------------------------

# Next, checking to see if those files exist on disk:
print('Checking each attachment(s)\' paths to confirm if they exist or not.... This may take a while - stand by!')
missingFiles = []
tic = time.time()
counter = 0
for line in pathsToCheck:
    counter = counter + 1
    line = line.strip()
    if not os.path.isfile(line):
        missingFiles.append(line)
    sys.stdout.write('\rCurrently checking if file # ' + str(counter) + ' out of ' + str(len(pathsToCheck)) + ' is actually present on the disk volume')
    sys.stdout.flush()

print(' ')
missingFilesCleaned = list(map(str.rstrip, missingFiles))
toc = time.time()
timeToGenerateMissingFilesList = round((toc - tic), 3)
print('There are ' + str(len(missingFilesCleaned)) + ' missing files')
print('--verifying each attachment(s)\' path took ' + str(timeToGenerateMissingFilesList) + ' seconds')
print(' ')

# Save the missing files to disk:
if len(missingFilesCleaned) > 0:
    print('Writing any missing files to MissingAttachments.txt...')
    listtodisk(missingFilesCleaned, 'MissingAttachments.txt')


# ------------------------------BUILD ATTACHMENTS DIR CONTAINS LIST SECTION---------------------------------------------

if len(missingFilesCleaned) > 0:
    # Should we attempt to look for lost attachments?
    print('This tool can attempt to find missing attachments located elsewhere inside the attachments directory')
    tocontinuefind = input_yesno('Would you like to find missing attachments now?')
    print('You answered ' + str(tocontinuefind))
    print(' ')

    if tocontinuefind:
        # delete Mac resource forks when found, if running on Mac platform (causes a 2nd full scan of AttachmentsDir)
        # but since Macs aren't generally used for production, their test data should usually be relatively small
        # commend out the next 2 lines if you happen to be working with a massive data set on a Mac
        if sys.platform.startswith('darwin'):
            os.system('find ' + attachmentPath + ' -name ".DS_Store" -exec rm -rf {} \;')

        # initialize variables
        attachmentsDirContains = []

        if len(sys.argv) != 1:
            arg = str(sys.argv[1])
            print("Python script parameter #1 : '" + arg + "'")
            print(' ')
            if arg == str('listfromdisk'):
                # Load previous os.walk results from disk ?
                print('--------------------------------------------------------------------------------------------------------')
                print('    os.walk is used to compile a list of files and paths in the attachment directory, which is then')
                print('      searched for missing files. Loading previous os.walk(attachmentDirectory) results can save time on')
                print('      very large instances if these discovery results have already been recorded.')
                print(' ')
                print('    --Note: os.walk using Python 3.5+ is considered to be 2-3 faster on Linux ')
                print('            and 8-9 faster on Windows than using Python 2.7')
                print(' ')
                print('    Previous copies of AttachmentsDirContains.txt are in the "results_backups" folder (ver 1.7.8+)')
                print(' ')
                print('    It\'s recommended to refresh the os.walk(attachmentDirectory) results unless they are very recent.')
                print('--------------------------------------------------------------------------------------------------------')
                print(' ')
                print('Would you like to load previous "AttachmentsDirContains.txt" results from disk?')
                print(' ')
                loadFromDiskYN = input_yesno('--press (y) to load from disk, press (n) to refresh the results) ')
                print('You answered ' + str(loadFromDiskYN))
                print(' ')
                if loadFromDiskYN:
                    # Load list from disk
                    print('Loading list from AttachmentsDirContains.txt')
                    print(' ')
                    AttachmentsDirContainsFile = open("AttachmentsDirContains.txt", "r")
                    attachmentsDirContainsRaw = AttachmentsDirContainsFile.readlines()

                    # add lines read from disk to the attachmentsDirContains list
                    for elements in attachmentsDirContainsRaw:
                        attachmentsDirContains.append(elements.strip('\n'))
                else:
                    print('Start the script over and do not pass any command line arguments to it to load list fresh from disk')
                    exit(0)
            else:
                print('Parameter "' + arg + '" not recognized, use "python3 FixMissingAttachments.py listfromdisk" to load previous AttachmentsDirContains.txt results')
                exit(1)
        else:
            # Build a new list from os.walk and write it to disk
            # reset the output file
            open('AttachmentsDirContains.txt', 'w').close()

            # start the clock
            tic = time.time()
            # datetime object containing current date and time
            now = datetime.now()
            # dd/mm/YY H:M:S
            dt_string = now.strftime("%B %d, %Y at %H:%M:%S")

            print('Building a list of your attachments directory. This may take a while - stand by!')
            print('--note: you can also tail the "AttachmentsDirContains.txt" file to observe the progress at this stage')
            print('Started on ' + dt_string)

            attachmentsDirContains = []

            # build list of files in the attachment directory using os.walk
            for pathroot, dirs, files in os.walk(attachmentPath):
                for filename in files:
                    attachmentsDirContains.append(os.path.join(pathroot, filename))
                    sys.stdout.write('\rCurrently on file # ' + str(len(attachmentsDirContains)) + ' of an unknown total number of files in the attachments directory')
                    sys.stdout.flush()
            print('\n')

            # write the list to disk
            print('Save AttachmentsDirContains.txt to disk?')
            print('--To save (potentially a lot of) disk space, only save this list if you plan on re-using these results later')
            print('--To load previous results from disk, execute "python3 FixMissingAttachments.py listfromdisk"')
            print(' ')
            saveToDisk = input_yesno('Save attachment directory contents to disk? ')
            print('You answered ' + str(saveToDisk))

            if saveToDisk:
                listtodisk(attachmentsDirContains, 'AttachmentsDirContains.txt')
                file_size = os.path.getsize('AttachmentsDirContains.txt')
                print('File size of "AttachmentsDirContains.txt" is: ' + str(file_size) + ' bytes')

            # stop the clock and report
            toc = time.time()
            timeToGenerateAttachDirList = round((toc - tic), 3)
            print('')
            print('List creation complete')
            print('--iterating the entire attachments directory took ' + str(timeToGenerateAttachDirList) + ' seconds')
            print('')

        # ----------------------------------------SEARCH SECTION--------------------------------------------------------

        # Now that we have our lists, let's lookup the missing attachments
        print('Searching for lost attachments.... This may take a while - stand by!')
        print(' ')
        now = datetime.now()
        dt_string = now.strftime("%B %d, %Y at %H:%M:%S")
        print('Started on ' + dt_string)

        foundFiles = []
        results = []
        count = 0
        ltsCounter = 0

        tic = time.time()

        # using AhoCorasick search https://github.com/abusix/ahocorapy/blob/master/src/ahocorapy/keywordtree.py

        # define a AhoCorasick KeywordTree for missing files along with the Keyword's original path
        # ...use to smartly search AttachmentDirContains list
        missingFileKeywordTree = KeywordTree(case_insensitive=True)
        for missing_file in missingFilesCleaned:
            missingFileKeywordTree.add(getKeyword(missing_file), missing_file)
        missingFileKeywordTree.finalize()

        # search through AttachmentDirContains.txt to see if there is a keyword match from missingFileKeywordTree
        for attachment_path in attachmentsDirContains:
            # update progress
            ltsCounter += 1
            sys.stdout.write('\rCurrently on file # ' + str(ltsCounter) + ' of ' + str(len(attachmentsDirContains)))
            sys.stdout.flush()

            # AhoCorasick search, result is a list of 2-tuples = - (missing_file_resultKeyword, original_missing_file)
            #                                                    - (missing_file_resultKeyword, original_missing_file)
            #                                                    ...and so on for all keyword hits on the search term
            result = missingFileKeywordTree.search_all(attachment_path)

            # immediately convert the result generator supplied by missingFileKeywordTree.search_all(attachment_path)
            # to a list for further list comprehension tactics
            result_list = list(result)

            # if a result exists, filter out extracted_text files and Mac resource forks from results
            if len(result_list) > 0:
                if not re.search('\\.extracted_text$', attachment_path):
                    if not re.search('\\.DS_Store$', attachment_path):
                        # now we have to finish creating the foundFiles list in "original file | found file" format
                        for i in range(len(result_list)):
                            # to handle situations where there are duplicate results, iterate through the list of tuples
                            # so you can address each tuple as a possible found file match, otherwise they are duplicate
                            result_sub_list = list(result_list[i])
                            # the positive result Keyword is in result_sub_list[0]
                            # the original missing_file that the Keyword was generated from is in result_sub_list[1]

                            # # un-comment the following to see what is about to be compared
                            # print('\noriginal missing file location: ' + str(result_sub_list[1]))
                            # print('possibly found this missing file at: ' + attachment_path + '\n')

                            # treat the values to compare as Path objects, not strings so as to avoid sub-string matches
                            # ...add full path of matches to the foundFiles list in "original_file|found_file" format
                            if Path(getKeyword(attachment_path)) == Path(getKeyword(result_sub_list[1])):
                                foundFiles.append(result_sub_list[1] + '|' + attachment_path)

        # write the foundFiles list to a file, or exit if there are no results
        if len(foundFiles) != 0:
            listtodisk(foundFiles, 'FoundAttachments.txt')
        else:
            print('')
            print('')
            print('Nothing found in the search')
            exit(0)

        print('')
        toc = time.time()
        timeSearchingForAttachments = round((toc - tic), 3)
        print('--searching the entire attachments directory took ' + str(timeSearchingForAttachments) + ' seconds')

        # Report the final total for files located elsewhere in the attachments directory, possibly recoverable
        print('Found ' + str(len(foundFiles)) + ' possible lost attachments - saved them to FoundAttachments.txt')
        print(' ')
    else:
        print('You answered ' + str(tocontinuefind))
        print(' ')
        print('Exiting without scanning for lost missing attachments.')
        sys.exit(0)

    # ----------------------------------------REPAIR SECTION-----------------------------------------------------------

    # Should we attempt to fix things?
    print('This tool can attempt to recover (move) any found attachments to their intended location.')
    tocontinuerecover = input_yesno('Would you like to attempt to recover missing attachments now?')
    print('You answered ' + str(tocontinuerecover))
    print(' ')
    if tocontinuerecover:
        tic = time.time()
        print('Attempting recovery... This may take a while - stand by!')
        recoveryCount = 0
        index = 0
        fileConflicts = []
        for index, comboPath in enumerate(foundFiles):
            # Get our source and destination:
            originalLocation = comboPath.split('|')[0]
            whereFound = comboPath.split('|')[1]

            # Do not continue if originalLocation actually exists
            if not os.path.exists(originalLocation):

                # Build a list of the file paths, along with possible extracted_text files
                originalFilenameWithExtractedTextFileList = [originalLocation, originalLocation + '.extracted_text']

                whereFoundFilenameWithExtractedTextFileList = [whereFound, whereFound + '.extracted_text']

                # Try to copy the attachment
                copyattachmentsuccess = True

                if not os.path.isdir(os.path.dirname(originalFilenameWithExtractedTextFileList[0])):
                    os.makedirs(os.path.dirname(originalFilenameWithExtractedTextFileList[0]))

                try:
                    shutil.copy2(whereFoundFilenameWithExtractedTextFileList[0], originalFilenameWithExtractedTextFileList[0])
                except IOError as io_err:
                    copyattachmentsuccess = False
                    print(whereFoundFilenameWithExtractedTextFileList[0] + ' not copied. Error: ' + str(io_err))
                except shutil.Error as errorMsg:
                    copyattachmentsuccess = False
                    print(whereFoundFilenameWithExtractedTextFileList[0] + ' not copied. Error: ' + str(errorMsg))
                except OSError as errorMsg:
                    copyattachmentsuccess = False
                    print(whereFoundFilenameWithExtractedTextFileList[0] + ' not copied. Error: ' + str(errorMsg))

                # Try and remove the found file if we copied it successfully:
                if os.path.isfile(originalFilenameWithExtractedTextFileList[0]):
                    os.remove(whereFoundFilenameWithExtractedTextFileList[0])

                # Now try and copy the extracted_text files if you can find them
                if os.path.isfile(whereFoundFilenameWithExtractedTextFileList[1]):
                    try:
                        shutil.copy2(whereFoundFilenameWithExtractedTextFileList[1], originalFilenameWithExtractedTextFileList[1])
                    except IOError as io_err:
                        copyattachmentsuccess = False
                        print(whereFoundFilenameWithExtractedTextFileList[1] + ' not copied. Error: ' + str(io_err))
                    except shutil.Error as errorMsg:
                        copyattachmentsuccess = False
                        print(whereFoundFilenameWithExtractedTextFileList[1] + ' not copied. Error: ' + str(errorMsg))
                    except OSError as errorMsg:
                        copyattachmentsuccess = False
                        print(whereFoundFilenameWithExtractedTextFileList[1] + ' not copied. Error: ' + str(errorMsg))

                    # Try and remove the found file if we copied it successfully:
                    if os.path.exists(originalFilenameWithExtractedTextFileList[1]):
                        os.remove(whereFoundFilenameWithExtractedTextFileList[1])

                if not os.listdir(os.path.dirname(whereFoundFilenameWithExtractedTextFileList[0])):
                    if sys.platform.startswith('darwin'):
                        os.system('find ' + attachmentPath + ' -name ".DS_Store" -exec rm -rf {} \;')
                    os.rmdir(os.path.dirname(whereFoundFilenameWithExtractedTextFileList[0]))

                if copyattachmentsuccess is True:
                    # Increment the counters
                    recoveryCount = recoveryCount + 1
                    index = index + 1
                    sys.stdout.write('\rRecovering File # ' + str(index) + ' of ' + str(len(foundFiles)))
                    sys.stdout.flush()
                else:
                    index = index + 1
                    print('Recovering File # ' + str(index) + ' of ' + str(len(foundFiles)) + ': Moving ....Failure')
            else:
                fileConflicts.append(whereFound + ' conflicted with existing ' + originalLocation + ', nothing moved')
                continue
                # exit(1)
                # the script used to exit here on detection of a file conflict but will now record it and move on

        toc = time.time()
        # We've completed!
        timeToRecoverAttachments = round((toc - tic), 3)

        # write any file conflicts to a file for review
        print(' ')
        print('The process has completed')
        if len(fileConflicts) > 0:
            print(str(recoveryCount) + ' attachments out of ' + str(len(foundFiles)) + ' have been successfully recovered (with ' + str(len(fileConflicts)) + ' file conflicts logged in "fileConflicts.txt").')
            print('\n--------------------------------------------------------WARNING--------------------------------------------------------')
            print('Some files that were attempted to be moved back in place were found to be in conflict with their intended destination.')
            print('-this may indicate that there are duplicate attachments.\n')
            print('We recommend using a comparison tool such as the following to compare conflicting files and confirm if they are indeed duplicates:')
            print('-on Linux: "diff" (https://man.archlinux.org/man/diff.1.en)')
            print('-on Windows: "windiff" (https://docs.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/how-to-use-windiff-utility)')
            print('')
            listtodisk(fileConflicts, 'fileConflicts.txt')
            print('See "fileConflicts.txt" for a full list of conflicts and what action was taken')
            file_size = os.path.getsize('fileConflicts.txt')
            print('File size of "fileConflicts.txt" is: ' + str(file_size) + ' bytes')
            print('--------------------------------------------------------WARNING--------------------------------------------------------\n')
        else:
            print(str(recoveryCount) + ' attachments out of ' + str(len(foundFiles)) + ' have been successfully recovered.')
        print('--recovering files took ' + str(timeToRecoverAttachments) + ' seconds')
        print(' ')
        print('NOTE: After recovering attachments you must clear your browser\'s cache and reload the page for any previously missing attachment to display properly again.')
        print('      Additionally (only if necessary) you can clear caches in the Cache Management portion of General Configururation')
        print('      ---but a restart of all nodes is a more through clearing of cache should you need to take that course of action.')
    else:
        print('Exiting without moving any files.')
        sys.exit(0)

else:
    print('Your attachments are healthy and in line with the database definitions!')

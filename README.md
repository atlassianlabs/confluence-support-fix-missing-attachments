# Confluence Support - Fix Missing Attachments Script
This is a missing attachment fixing tool designed to identify, find, and repair attachments that are missing from their expected location in the attachment folder. Attachments which are missing from their expected location will be displayed by Confluence as _"attachment not found"_. See https://confluence.atlassian.com/confkb/how-to-resolve-missing-attachments-in-confluence-201761.html for more information. 

The `FixMissingAttachments.py` script uses the data you provide in `Attachments.txt` to compute the intended filesystem paths, checks if they really exist at that location, records any missing attachments, searches for missing attachments that can be recovered, and offers to move any found attachments to their intended location. 

### Important note if using Python2: 
* As of version 1.9.0, Python2 clients will be required to install the `pathlib` library with the following command (`pathlib` is native in Python3):
    * `pip install pathlib` or `<path_to_Python2>\python -m pip install pathlib`
* The `os.walk` functionality (which is used to iterate the contents of the attachments directory) in Python 3.5+ is _2-3 faster_ than Python2 on Linux and _8-9 times faster_ than Python2 on Windows
    * **It's highly recommended to use Python version 3.5+**

## What's new in 1.9.0
* Even faster search by reducing the complexity in how we handle processing results.
* Input validation added to all yes/no prompts.
* File sizes for saved files are now displayed as a confirmation (will be > 0 bytes).
* Fixed bug where only version 1-9 of an attachment was recoverable, the rest reported as being in conflict. 
    * This bug was introduced when an expensive RegEx expression was replaced with a simple string comparison, resulting in some unintended sub-string matches that are reported as conflicts. In fixing this bug, this simple comparison will still be made (instead of the older RegEx method), only now by treating the path strings as _Path_ objects from the [pathlib](https://docs.python.org/3/library/pathlib.html) library we are guaranteed there will be no sub-string match false alarms. 
        * **Note**: While `pathlib` is native in Python3, Python2 clients will be required to install the [pathlib](https://docs.python.org/3/library/pathlib.html) library with the following command:
      
    `pip install pathlib` or `<path_to_Python2>\python -m pip install pathlib`

## What's new in 1.8.1
* `disk` command line option renamed to `listfromdisk` for clarity.
* Added logic to handle extra whitespaces around the attachment version in the input file.
* The script will now log file conflicts and continue rather than stop. 
    * Conflicts are logged in `fileConflicts.txt` and nothing is moved. 
    * We recommend using a tool like [diff](https://man.archlinux.org/man/diff.1.en) on Linux or [windiff](https://docs.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/how-to-use-windiff-utility) on Windows to compare and confirm if they are actually duplicates of each other or not.

## What's new in 1.8.0
* **_2-3 times faster results_** with new [Aho–Corasick](https://github.com/abusix/ahocorapy/blob/master/src/ahocorapy/keywordtree.py) search.
    * See [Aho–Corasick Algorithm](https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm) for more details on the new search.
    * See [ahocorapy](https://github.com/abusix/ahocorapy) for the python libraries used (no compiled C library required).
* More timers added at various stages to help troubleshoot where time is being spent in large instances.
* Rather than prompt every time, the functionality to re-use the `AttachmentsDirContains.txt` file has been set to be triggered by adding a `disk` parameter when running the script:
    * run `python3 FixMissingAttachments.py listfromdisk` to load `AttachmentsDirContains.txt` results from disk.

## What's new in 1.7.8
* You can now re-use the `AttachmentsDirContains.txt` file between runs. This can drastically reduce the total time to run the script if the previous results are recent. 
    * Previous copies are tar+gz'd in the `results_backups` folder, just restore that file to the script's folder, run the script and say you want to "load results from disk" when prompted. 
    * `AttachmentsDirContains.txt` results are backed up as of version 1.7.8 forward.

## What's new in 1.7.6
* The Fix Missing Attachments Script now searches for and recovers **all versions** of attachments. 
* If allowed it will now also move their corresponding "extracted_text" files (if any exist) when recovering found attachments.

## What's new in 1.7.5
 * The Fix Missing Attachments Script now searches for the version of the attachment specifically rather than detect the presence of the attachment's folder alone. This is to improve the ability to detect recoverable files located elsewhere in the attachments directory as well as to identify missing files in the following conditions where:
     * the attachment folder is missing
     * the attachment folder is empty
     * the attachment folder is present and not empty, but does not contain the desired version *(new feature!)*
 * Timers and progress bars have been introduced in order to report the time it took to complete various functions. This can aid in diagnosing where all the time is being spent when running the script on very large instances with very many attachments, as well as to help confirm the script is in fact still running. 
 * The script now reads the list of folders within the attachments directory, the list of missing files and the list of found file all from memory rather than from disk. The script still writes these lists to disk for future reference and those results are tar+gz'd for backup each time the script is executed.
 * Since missing attachment detection is now granular down to the file-version level, empty folders are no longer detected as false-positives and thus the previous empty-folder-renaming functionality in the script and the corresponding `DeleteEmptyDirectories.py` script were removed. 

## What's new in 1.7.0
 * Previously, empty attachment directories found in their expected locations were erroneously counted as containing an attachment (even if empty).
 * The script previously would reset the output files at the start of each new execution. Since running the script can take a very long time to complete on very large attachments folders, the resulting `*.txt` output files from previous runs will now be backed up to a `.tar.gz` file in the `results_backups` directory each time you run the script before resetting the output files for the next execution.
 * Additional (y/n) prompts were introduced so admins can run the script with the intention of only using it to **identify** missing files and search for them to see what could be recovered, but not necessarily recover (move) any attachments at that time. This gives admins the flexibility to audit the results recorded in `MissingFiles.txt` and `FoundFiles.txt` prior to allowing the scripts to automatically recover missing attachments or delete empty directories.
 * MacOS detection added to the empty directory identification routine. The scripts will safely delete any ".DS_Store" files they encounter when running on MacOS so truly empty directories in the attachment folder are not erroneously missed during detection.
 * Python3 compatibility added, maintained Python2 compatibility.

## What are these scripts used for?
The `FixMissingAttachments.py` script can search for and recover found attachments but will always prompt (y/n)  before moving anything, therefore you can safely use these scripts to do the following:

 * Identify missing attachments by computing the intended file system paths for the data you provide in `Attachments.txt` and then checking the file system to confirm if the directory actually exists or not in that location.
   - the script records missing attachments in `MissingAttachments.txt`.
 * Search for any missing attachments that can be located in other locations in Confluence's attachments folder.
   - the script records found attachments in `FoundAttachments.txt`.
 * Recover (move) any found attachments back to their expected location.

The Fix Missing Attachments Script can be used safely to generate the `.txt` output files, which you can then use to manually review before allowing the script to recover any found missing attachments. 


## Disclaimer
* Ensure Confluence has been shut down and that you have a full file system backup.
* Do not run this in production environments without running it in a test environment first!

## Instructions (Confluence 5.7 and above):
Run the following SQL statement which will generate a list of all versions of all attachments:

    SELECT SPACEID,
    PAGEID,
    CONTENTID,
    CONTENT.VERSION
    FROM   CONTENT
    WHERE  CONTENTTYPE = 'ATTACHMENT'
    AND    SPACEID IS NOT NULL
    AND    PREVVER IS NULL
    UNION
    SELECT SPACEID,
    PAGEID,
    PREVVER,
    CONTENT.VERSION
    FROM   CONTENT
    WHERE  CONTENTTYPE = 'ATTACHMENT'
    AND    SPACEID IS NOT NULL
    AND    PREVVER IS NOT NULL;

You can alternatively limit to a specific space by name (be sure to update your desired space name in both places)...

    SELECT C.SPACEID,
           C.PAGEID,
           C.CONTENTID,
           C.VERSION
    FROM   CONTENT C
    JOIN   SPACES S
    ON     S.SPACEID = C.SPACEID
    WHERE  C.CONTENTTYPE = 'ATTACHMENT'
    AND    C.SPACEID IS NOT NULL
    AND    C.PREVVER IS NULL
    AND    S.SPACENAME = 'Demonstration Space'
    UNION
    SELECT C.SPACEID,
           C.PAGEID,
           C.PREVVER,
           C.VERSION
    FROM   CONTENT C
    JOIN   SPACES S
    ON     S.SPACEID = C.SPACEID
    WHERE  C.CONTENTTYPE = 'ATTACHMENT'
    AND    C.SPACEID IS NOT NULL
    AND    C.PREVVER IS NOT NULL
    AND    S.SPACENAME = 'Demonstration Space';

... or by space key (please update space key in both places):

    SELECT C.SPACEID,
           C.PAGEID,
           C.CONTENTID,
           C.VERSION
    FROM   CONTENT C
    JOIN   SPACES S
    ON     S.SPACEID = C.SPACEID
    WHERE  C.CONTENTTYPE = 'ATTACHMENT'
    AND    C.SPACEID IS NOT NULL
    AND    C.PREVVER IS NULL
    AND    S.SPACEKEY = 'DS'
    UNION
    SELECT C.SPACEID,
           C.PAGEID,
           C.PREVVER,
           C.VERSION
    FROM   CONTENT C
    JOIN   SPACES S
    ON     S.SPACEID = C.SPACEID
    WHERE  C.CONTENTTYPE = 'ATTACHMENT'
    AND    C.SPACEID IS NOT NULL
    AND    C.PREVVER IS NOT NULL
    AND    S.SPACEKEY = 'DS';

### Attachments.txt File
Once you have the SQL results, export them into a text file named `Attachments.txt` (the case sensitivity of the file name is important). Do not include header rows in your export.

## Executing the scripts
The Fix Missing Attachments Script should be executed **as the same user that launched the Confluence java process**. This ensures the proper file permissions.

Run `python3 FixMissingAttachments.py` and follow the prompts.

* You will be prompted (y/n) at each phase of the execution, including before any directories are renamed or moved.
* The script will prompt you for the separator you choose to export with.
    - Type `t` character to use a tab, otherwise, type the character you're using such as `,` or `|`. That character will be used to split each line.

After recovering attachments you must **clear your browser's cache and reload the page** for any previously missing attachment to display properly again.

The Fix Missing Attachments Script can be used safely to generate the `.txt` output files, which you can then use to manually review before allowing the script to recover any found missing attachments.

Run the script a 2nd time after recovering attachments to identify any remaining missing attachments. Attachments which cannot be located and recovered using the script should be restored from external backup.

### Python versioning
**`python3` is recommended** over `python` (ver 2.x) due to performance improvements in `python3` as well as it having all required modules natively.

If you are using `python` (ver 2.x) :

* **and** you cannot execute the scripts due to a missing required python component
* **and** you are unable to use `python -m pip install <module>` to install the missing component due to a rights restriction, not having internet access, or some other reason...
    * `python3` should be used instead as it natively comes with all the necessary requirements.

Otherwise, use `python -m pip install <module>` to install the missing component.


## Additional Notes
 * Run the script a 2nd time after recovering attachments to identify any remaining missing attachments.
 * Attachments which cannot be located and recovered using the script should be restored from external backup.    
 * After recovering attachments you must clear your browser's cache and reload the page for any previously missing attachment to display properly again. 

### Reasons attachments can go missing
If a page with attachments is moved to another space, the attachments will need to move from the current space's directory to the destination space's directory. **This does not occur** when a page is moved within the same space. This is due to Confluence's "**Hierarchical File System Attachment Storage**" which determines an attachment's location on the filesystem based on an attachment's `spaceid`, `pageid` and `attachmentid`. 

 * See https://confluence.atlassian.com/doc/hierarchical-file-system-attachment-storage-704578486.html for more information on Confluence's Hierarchical File System Attachment Storage model to become familiar with the directory structure of the attachments folder and how the paths are computed. 

Failed page moves _(where the attachment folders are moved to their destination space's location but the database transaction is rolled back for any reason)_ are a common cause of missing attachments although there could be other reasons, including external automation or other processes outside of Confluence acting on the attachments folder.
 
### How does the script search for and recover missing attachments?
Since attachment IDs do not change during page moves and are used as a part of the directory structure, we can determine where an attachment should be from the SQL data exported to `Attachments.txt`. 

The script will then (if allowed) search the entire attachments directory for any missing attachments that are found elsewhere in the attachment directory's folder structure. The script records missing attachments in `MissingAttachments.txt` and found attachments in `FoundAttachments.txt`

### Manually Searching for Missing Attachments
The script will search for the version of each attachment listed in `Attachments.txt`. By default, the SQL queries used in the instructions below will pull the current version of the attachment when listing what to search for. 

Previous versions of any attachment can be searched for manually by their attachmentID.

* Obtain the attachmentID from the `CONTENT` table of the database for the desired attachment's previous version. 
* Use your OS's native search tools and search the entire attachment directory for the previous version's attachmentID.
* If located, it will return the path to a folder which is named the same as the attachmentID. Inside it will be the actual attachment files, each named corresponding to the version of the attachment (`1`,`2`,`3`, and so on...)
* Inspect the attachment folder for the desired previous version of the missing attachment. 
* Move the missing attachment file into the correct folder location to restore it.

Note: There may also be "extracted_text" files present in the attachment folder which are created by the Lucene indexer, do not move them - instead, reindex the site once all attachments have been restored for the restored attachments' contents to be indexed and searchable.

## Need a hand?
Problems? Attach the `*.txt` files in this directory to your support ticket, and your engineer will be able to help you out. You can also raise an issue with the scripts here!
